<?php
/*
 * Template Name: BulkyStock Home Page
 */
get_header();
?>

<style>
	.header-search-bar-hero__text{
		margin-bottom: 30px
	}
	.header-search-bar-hero__text--main{
		font-size: 41px;
	}

	.header-seach-filter-area form ul {
		display: flex;
		align-items: stretch;
		background: #fff;
	}

	.header-seach-filter-area form ul li{
		padding-right: 0px;
	}

	.header-seach-filter-area form ul>li:nth-child(1) {
		border-right: 1px solid #ccc;
		flex-grow: 10;
	}
	.header-seach-filter-area form ul>li:nth-child(2) {
		flex-grow: 1;
		padding-right: 20px;
	}
	.header-seach-filter-area form ul>:nth-child(3){
		flex-grow: 1;
	}

	.header-seach-filter-area form ul>li input[type=text] {
		border: none;
		height: 100%;
		width: 100%;
		padding: 20px 30px 20px 30px;
	}

	.header-seach-filter-area form ul>li select {
		border: none;
		height: 100%;
		width: 100%;
		padding: 20px 5px 20px 20px;
	}
	.header-seach-filter-area form ul>li input[type=submit] {
		border: none;
		height: 100%;
		width: 100%;
		padding: 20px 5px 20px 20px;
	}

	.catagory-listing-menu12 {
		position: absolute;
		top: 0;
		right: 50px;
		z-index: 111;
		width: 200px;
		height: 100%;
    }
	.catagory-listing-menu12 ul{
		list-style: none;
		text-align: right;
	}
	.catagory-listing-menu12 h4{
		text-align: right;
		padding: 10px 0;
		font-weight: 600;
	}
	.catagory-listing-menu12 ul li{
		padding: 4px 0;
	}
	.catagory-listing-menu12 ul li a {
		color:#31708f;
	}
	.catagory-listing-menu12 ul li a:hover {
      background: #eceff1;
	  display: block;
    }

</style>

<header class="header-search-bar__hero-section" style="background-color: rgb(206, 226, 215); height: 272px;padding: 50px;text-align: center;position:relative">
   <div class="header-serch-area">
		<div class="header-search-bar-hero__text">
			<h2 class="header-search-bar-hero__text--main">Find Your image with Format</h2>
		</div>
		<div class="header-seach-filter-area">
			<?php echo do_shortcode('[searchandfilter fields="search,image_format" hierarchical=",1" submit_label="Search"]'); ?>
		</div>
	</div>
<!--
	<div class="catagory-listing-menu12">
		<h4>Categories</h4> -->
		<?php
// wp_nav_menu(array(
//     'theme_location' => 'listing_menu',
//     'menu' => 'Listing Menu',
//     'menu_class' => 'listing',
//     'menu_id' => 'listing-menu',
//     'fallback_cb' => 'wp_page_menu',
//     'items_wrap' => '<ul id = "%1$s" class = "%2$s">%3$s</ul>',
// ));
?>
	<!-- </div> -->
</header>







<?php //get_template_part( 'loop-templates/partials/product', 'category-child'); ?>



<section class="content-area" style="background: #f5f5f5">
	<div class="container">
		<div class="row">
			<div class="col-md-12">



				<?php //get_template_part( 'loop-templates/partials/product', 'category'); ?>




				<!-- <ul class="landing-top-menu scroll-x--hidden">
					<li class="landing-top-menu__item">
						<a class="text-link--sly action--with-emphasis" data-nav="nav_Creative" data-ui-location="landing_page_category_tabs" href="/creative-images" target="_self">CREATIVE</a>
					</li>
					<li class="landing-top-menu__item">
						<a class="text-link--sly action--with-emphasis is-selected" data-nav="nav_Editorial" data-ui-location="landing_page_category_tabs" href="/editorial-images" target="_self">EDITORIAL</a>
					</li>

					<li class="landing-top-menu__item">
						<a class="text-link--sly action--with-emphasis" data-nav="nav_Video" data-ui-location="landing_page_category_tabs" href="/creative-video" target="_self">VIDEO</a>
					</li>
					<li class="landing-top-menu__item">
						<a class="text-link--sly action--with-emphasis" data-nav="nav_Music" data-ui-location="landing_page_category_tabs" href="/music" target="_self">MUSIC</a>
					</li>
				</ul>


				<ul class="landing-secondary-menu scroll-x--hidden box--grey">
					<li class="landing-secondary-menu__item">
						<a class="landing-secondary-menu__link landing-secondary-menu__link--unselected text-link--sly" data-nav="nav_Editorial_EditorialPhotos" data-ui-location="landing_page_subcategory_tabs" href="https://www.gettyimages.com/editorial-images/all" target="_self">All</a>
					</li>
					<li class="landing-secondary-menu__item">
						<a class="landing-secondary-menu__link btn--grey" data-nav="nav_Editorial_News" data-ui-location="landing_page_subcategory_tabs" href="https://www.gettyimages.com/editorial-images/news" target="_self">News</a>
					</li>
					<li class="landing-secondary-menu__item">
						<a class="landing-secondary-menu__link landing-secondary-menu__link--unselected text-link--sly" data-nav="nav_Editorial_Sports" data-ui-location="landing_page_subcategory_tabs" href="https://www.gettyimages.com/editorial-images/sport" target="_self">Sports</a>
					</li>
					<li class="landing-secondary-menu__item">
						<a class="landing-secondary-menu__link landing-secondary-menu__link--unselected text-link--sly" data-nav="nav_Editorial_Entertainment" data-ui-location="landing_page_subcategory_tabs" href="https://www.gettyimages.com/editorial-images/entertainment" target="_self">Entertainment</a>
					</li>
					<li class="landing-secondary-menu__item">
						<a class="landing-secondary-menu__link landing-secondary-menu__link--unselected text-link--sly" data-nav="nav_Editorial_Archival" data-ui-location="landing_page_subcategory_tabs" href="https://www.gettyimages.com/editorial-images/archival" target="_self">Archival</a>
					</li>
				</ul> -->










				<?php //get_template_part( 'loop-templates/partials/popular', 'product'); ?>

				<?php //get_template_part( 'loop-templates/partials/product', 'category'); ?>

				<?php get_template_part('loop-templates/partials/product', 'justForYou');?>

			</div><!--.col-md-12-->
		</div><!--.row-->
	</div><!--.container-->
</section>

<?php
get_footer();
