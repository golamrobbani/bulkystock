<?php
/**
 * @package themeplate
 */
?>

<style>
	.fancy-grid div.clipper {
		display: flex;
		flex-wrap: wrap;
	}
	.fancy-grid div.clipper>article {
		flex: 1 0 21%;
		margin: 10px 0;
	}
	.fancy-grid div.clipper a {
		position: relative;
		flex: 1 1;
		width: auto;
		background-size: cover;
		background-position: 50%;
		display: block;
	}
	.fancy-grid figure {
		position: relative;
		height: 350px;
		margin: 0;
	}
	.fancy-grid figure img {
		display: none;
		max-width: 100%;
	}
	.fancy-grid figure figcaption {
		position: absolute;
		z-index: 5;
		bottom: 0;
		width: 100%;
		color: #fff;
		padding: 50px 25px 25px;
		font-size: 18px;
		background: linear-gradient(180deg,transparent 0,rgba(8,8,8,.6));
	}
	.fancy-grid figure figcaption .category-tag {
		font-size: 12px;
		color: #fff;
		font-weight: 700;
		text-transform: uppercase;
	}
	.editorial-landing__title {
		margin: 4em 1.25em 2.5em 2em;
	}
</style>

<?php 
$taxonomyName = "download_category"; 
$parent_terms = get_terms( $taxonomyName, array( 'parent' => 0, 'orderby' => 'slug', 'hide_empty' => false ) );   

echo '<section class="fancy-grid">';
foreach ( $parent_terms as $pterm ) {
	$p_category_link = get_term_link($pterm->term_id,'download_category');

	echo '<div class="editorial-landing__title"><h2><a class="" href="'. $p_category_link.'">'.$pterm->name.'</a></h2></div>';


	echo '<div class="clipper">';
	$terms = get_terms( $taxonomyName, array( 'parent' => $pterm->term_id, 'orderby' => 'slug', 'hide_empty' => false ) );
	foreach ( $terms as $term ) {
      $term_image = get_term_meta( $term->term_id, '_category_image', true );

		echo '<article>
		<a style="background-image: url('.$term_image.'); background-position: center top;" class="active" href="'.get_term_link( $term ).'">
		<figure>
		<img alt="'.$term->description.'" src="'.$term_image.'">
		<figcaption>
		<div class="category-tag">'.$term->name.'</div>'
         .$term->description.
		'</figcaption>
		</figure>
		</a>
		</article>';
	}
	echo '</div>';
}
echo '</section>';

?>

